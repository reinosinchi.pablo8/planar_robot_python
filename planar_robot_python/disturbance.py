import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
from custom_messages.msg import KinData
from custom_messages.msg import CartesianState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np

class Disturbance(Node):
    def __init__(self):
        super().__init__('disturbance')

        self.publisher_                     = self.create_publisher(
            JointState, 
            'qdot_disturbance', 
            10)
        
        
    
        self.declare_parameter('A',1)
        self.declare_parameter('w',10)

        self.initialized        = False        
        
        self.sampling_period    = 1e-2
        self.qdot               = np.zeros(2)

        self.timer              = self.create_timer(self.sampling_period, self.timer_callback)
        self.dt = 0.0
        

    def timer_callback(self):
        A = self.get_parameter('A').value
        w = self.get_parameter('w').value

        joint_state                 = JointState()
        self.dt += self.sampling_period 
        self.qdot[0]  = A* np.sin(w*self.dt)
        self.qdot[1]  = A* np.sin(w*self.dt)

        joint_state.velocity        = [self.qdot[0], self.qdot[1]]
        self.publisher_.publish(joint_state)
        
def main(args=None):
    rclpy.init(args=args)
    disturbance = Disturbance()
    rclpy.spin(disturbance)
    disturbance.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()

