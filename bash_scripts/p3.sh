. install/setup.zsh
ros2 run planar_robot_python trajectory_generator &
ros2 run planar_robot_python high_level_manager &

sleep 2
echo "\n=============================================================="
echo "[Enter] ros2 topic pub /kin_data custom_messages/msg/KinData {x: 0.0, y: 0.0} --once"
echo "[Enter] ros2 topic pub  /launch std_msgs/msg/Bool {data:true} --once "
read 
ros2 topic pub /kin_data custom_messages/msg/KinData "{x: 0.0, y: 0.0, jacobian:[0.0,0.0,0.0,0.0]}" --once
ros2 run plotjuggler plotjuggler &

sleep 2
echo "\n=============================================================="
echo "------> Load plotjuggler/p2_layout.xml on PlotJuggler"
read 
ros2 topic pub  /launch std_msgs/msg/Bool "{data: true}" --once

sleep 2
echo "\n=============================================================="
echo "[Enter] ros2 topic pub /kin_data custom_messages/msg/KinData {x: 1.0, y: 1.0} --once"
read 
ros2 topic pub /kin_data custom_messages/msg/KinData "{x: 1.0, y: 1.0, jacobian:[0.0,0.0,0.0,0.0]}" --once

sleep 2
echo "\n=============================================================="
echo "[Enter] ros2 topic pub /kin_data custom_messages/msg/KinData {x: 1.6, y: 0.0} --once"
read 
ros2 topic pub /kin_data custom_messages/msg/KinData "{x: 1.6, y: 0.0, jacobian:[0.0,0.0,0.0,0.0]}" --once

sleep 2
echo "\n=============================================================="
echo "[Enter] ros2 topic pub /kin_data custom_messages/msg/KinData {x: 0.0, y: 1.6} --once"
read 
ros2 topic pub /kin_data custom_messages/msg/KinData "{x: 0.0, y: 1.6, jacobian:[0.0,0.0,0.0,0.0]}" --once

sleep 2
echo "\n=============================================================="
echo "[Enter] ros2 topic pub /kin_data custom_messages/msg/KinData {x: -1.6, y: 0.0} --once"
read 
ros2 topic pub /kin_data custom_messages/msg/KinData "{x: -1.6, y: 0.0, jacobian:[0.0,0.0,0.0,0.0]}" --once

sleep 2
echo "\n==============================================================\n"
echo "[Enter] kill all ROS2 processes"
read 
killall trajectory_generator 
killall plotjuggler
killall high_level_manager