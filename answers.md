# P2 answer
## Merge Comments
Lors de ce devoir nous avons appris a crée un node 

## Result Comments
Le code fonctionne comme prévu et affiche les mêmes résultat que sur le gif (hormis quelques warnings)



# P3 answer
## Merge Comments
Dans le code il y a quelques différences comme l'ajout d'une fonction en plus generator_trajectory, cette partie aurait pu directement être faite dans le callback.

## Result Comments
Le code ne fonctionnait pas comme prevu, je n'ai donc pas obtenu les résultats attendus. Cette erreur peut être du à la partie read_trajectory_from_file qui n'est pas nécessaire, le fichier ne doit pas être lu car on reçoit les informations par un message, nous n'avons donc pas besoin de chercher un fichier, cette partie étant fausse elle pourrait poser d'éventuels problèmes par la suite.



# P4 answer
## Merge Comments
Etant donné que le code ne fonctionnait pas j'ai décidé de prendre celui donné par le professeur afin d'avoir une version correcte.

## Result Comments
Le code a été donné par le professeur.


# P5 answer
## Merge Comments
Le code a été donné par le professeur.
## Result Comments

Le code de controller ne fonctionnait pas car il manquait le timer et le reste du code ne permettait pas d'obtenir le résultat obtenu pareillement pour simulator.


# P6 answer

## Merge Comments
Après merge j'ai remarqué que je n'avais pas très bien compris la consigne donc mon code ne fonctionnait pas comme prévu, j'ai décidé de prendre celui donné par le professeur afin d'avoir une version correcte.

## Result Comments
Nous obtenons bien la simulation du bras. Dans un premier temps j'ai simulé sans bruit et le bras faisait bien ce qui était attendu, dans un second temps j'ai rajouté le bruit à une fréquence de 100 hz et une amplitude de 10 dans ce cas le bras ne s'arrêtait pas ce qui implique que le bruit était trop élevé pour être compensé. Puis après quelques tests le bras ne s'arrête toujours pas avec une amplitude de 0.1 et une fréquence de 100 hz, cela peut être du à la mauvaise déclaration de la fonction bruit l'équation qui est peut-être mal implémentée.
Apres test j'ai trouvé que pour rajouter le bruit il faut rajouter sous forme d'integrale afin d'avoir une position car on la considère comme une vitesse et non comme une position 
